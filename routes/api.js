const express = require('express');
const router = express.Router();
const sessionController = require("../controllers/session-controller");
const {checkcookie} = require("../middlewares/checkcookie");

router.get('/startsession', checkcookie, sessionController.startsession);
router.post('/addtask', sessionController.addTasks);
router.get('/gettasks', sessionController.getTasks);

module.exports = router;