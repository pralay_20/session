const express = require("express");
const {PORT} = require("./config");
const cors = require("cors");
const app = express();
const cookieParser = require('cookie-parser');
app.use(cookieParser()); //just like body-parser
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.set('view engine', 'ejs');
const apiroutes = require("./routes/api");

app.use('/api', apiroutes); //any incoming request with '/api' comes route it to apiroutes


app.listen(PORT, ()=>{
    console.log("Server started at ", PORT);
})