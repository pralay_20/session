const {MONGO_CONNECT_URL} = require("../config/index");
const mongoose = require("mongoose");

mongoose.connect(MONGO_CONNECT_URL, { useNewUrlParser: true, useUnifiedTopology: true, dbName: "Flair_apis" });
const sessionSchema = new mongoose.Schema({
    _id: String,
    task_urls: [
        {
            urls: String,
            status: {
                type:String,
                enum: ['PENDING', 'IN-PROCESS', 'COMPLETED', 'FAILED'],
                default:'PENDING'

            }
        }
    ]


});
const Session = mongoose.model('Session', sessionSchema);

module.exports = Session