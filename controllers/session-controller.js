const Session = require("../models/model")
const { v4: uuidv4 } = require('uuid');
const express = require("express");
const app = express();


const cookieParser = require('cookie-parser');
app.use(cookieParser()); //just like body-parser

const startsession = async(req, res) => {
    try{
        const sessionId = uuidv4();
        const newsession = new Session({_id:sessionId});
        newsession.save();
        res.cookie("session", sessionId);
        res.render('index', {sessionId: `${sessionId}`});
    }
    catch (error){
        console.log(error);
        return res.status(501).json({
            data:{},
            success:false,
            err: error
        })
    }
}

const addTasks = async(req, res) => {
    try{
        const {url} = req.body; //destructed the object
        const session = req.cookies.session;
        const check_session = await Session.findById(session);
        if(check_session){
            /**
             * Agar session present hoga DB me toh uss session me tasks (URL) add karo
             */
            const newObj = {urls: url};
            Session.findByIdAndUpdate(session,{$push:{task_urls: newObj}}).exec();
            return res.status(201).json({
                msg:"success"
            })
        }
        else{
            res.status(500).json({
                msg:"Something went wrong"
            })
        }
    }catch(error){
        console.log(error);
        return res.status(501).json({
            data:{},
            success:false,
            err: error
        })
    }
}


const getTasks = async (req, res) => {
    try{
        const session = req.cookies.session;
        const data = await Session.findOneAndUpdate({_id: `${session}` ,'task_urls.status': 'PENDING'},{ $set: { 'task_urls.$.status': 'IN-PROCESS' } },{projection:{ 'task_urls.$': 1 }});
        if(data){
            const url = data.task_urls[0].urls;
            console.log(url);
            return res.status(200).json({
                data:url,
                msg:"success"
            });
        }
        else{
            return res.status(500).json({
                data:{},
                msg:"failed"
            })
        }
    }catch(error){
        console.log(error);
    }
}


module.exports = {
    startsession,
    addTasks,
    getTasks
}