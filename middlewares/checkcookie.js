
// const express = require("express");
// const app = express();


// const cookieParser = require('cookie-parser');
// app.use(cookieParser()); //just like body-parser



const checkcookie = async (req, res, next) => {
    const cookie = req.cookies.session;
    if(cookie){
        //session is present, so dont generate a new sessionId
        res.render('index', {sessionId: `${cookie}`});
    }
    else{
        next();
    }
}

module.exports = {
    checkcookie
}